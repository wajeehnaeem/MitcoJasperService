package com.jasperservice.service.exceptionmappers

import javax.ws.rs.NotFoundException
import javax.ws.rs.ext.ExceptionMapper
import javax.ws.rs.ext.Provider
import javax.ws.rs.core.Response

//Exception handler mechanism
@Provider
public class NotFoundExceptionHandler : ExceptionMapper<NotFoundException>
{
    override fun toResponse(e: NotFoundException?): Response
    {
        println("Just handled this shit.")
        return Response.status(Response.Status.NOT_FOUND).build()
    }

}
//endregion

