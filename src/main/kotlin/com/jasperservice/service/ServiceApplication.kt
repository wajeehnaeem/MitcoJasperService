package com.jasperservice.service
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer


@SpringBootApplication
@EnableAutoConfiguration(exclude = [DataSourceAutoConfiguration::class,
    DataSourceTransactionManagerAutoConfiguration::class,
    HibernateJpaAutoConfiguration::class,
    SecurityAutoConfiguration::class])
class ServiceApplication : SpringBootServletInitializer() {
   companion object {
       @JvmStatic
       fun main(args: Array<String>)
       {
           SpringApplication.run(ServiceApplication::class.java, *args)
       }
   }
    override fun configure(builder: SpringApplicationBuilder?): SpringApplicationBuilder = builder!!.sources(ServiceApplication::class.java)
}

