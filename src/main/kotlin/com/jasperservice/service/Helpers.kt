package com.jasperservice.service

import com.jasperservice.service.models.DatabaseCredentials
import java.sql.Connection
import java.sql.DriverManager
import java.util.*

data class ResourceParameters (var username: String, var password: String, var resourceURL: String)
class Helpers
{
    companion object
    {
        fun PrepareURLAndAuthenticationParameters(ReportName: String, format: String) : ResourceParameters
        {
            var username = System.getenv("J_USERNAME")
            var password = System.getenv("J_PASSWORD")
            var jasperRestURL = System.getenv("J_RESTURL")
            var reportIncludingFormat = "$ReportName.$format"
            var resourceURL = "$jasperRestURL/reports$reportIncludingFormat"
            return ResourceParameters(username, password, resourceURL)
        }

        fun PrepareReportListingAndAuthenticationParameters() : ResourceParameters
        {
            var username = System.getenv("J_USERNAME")
            var password = System.getenv("J_PASSWORD")
            var jasperRestURL = System.getenv("J_RESTURL")
            var path = System.getenv("PATH")
            var reportListingURL = "$jasperRestURL/resources"
            println(username)
            println(password)
            println(jasperRestURL)
            println(path)

            return ResourceParameters(username, password, reportListingURL)

        }

        fun PrepareConnection(dbParameters: DatabaseCredentials) : Connection
        {
            var dbConnectionProperties = Properties()
            dbConnectionProperties.put("user", dbParameters.User)
            dbConnectionProperties.put("password", dbParameters.Password)
            Class.forName("com.mysql.jdbc.Driver").newInstance()
            var connectionString = "jdbc:mysql://${dbParameters.Host}/${dbParameters.Database}"
            println(TimeZone.getDefault())
            return DriverManager.getConnection(connectionString, dbParameters.User, dbParameters.Password)

        }
    }
}