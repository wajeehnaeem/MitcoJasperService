package com.jasperservice.service.models

data class Resource(
		var resourceLookup: List<ResourceLookup?>?
)

data class ResourceLookup(
		var version: Int?,
		var permissionMask: Int?,
		var creationDate: String?,
		var updateDate: String?,
		var label: String?,
		var description: String?,
		var uri: String?,
		var resourceType: String?
)
data class ResourceDescriptor(var resourceType: String?, var resourceName: String?, var resourceUri: String?)

data class ReportParameters
(
		var ReportPath: String,
		var ReportDBConfPath: String,
        var ReportFormat: String?,
        var ReportParams : Map<String, Object>?,
		var ReportSavePath : String,
		var ReportSaveName : String,
		var ExportToSeperatePages: Boolean?
)
data class DatabaseCredentials
(
		var User: String?,
        var Password: String?,
        var Host: String?,
        var Database: String?,
        var Port: Int?
)