package com.jasperservice.service.controllers

import com.fasterxml.jackson.module.kotlin.*
import com.jasperservice.service.Helpers
import com.jasperservice.service.models.*
import net.sf.jasperreports.engine.*
import net.sf.jasperreports.engine.export.*
import net.sf.jasperreports.export.*
import org.springframework.http.*
import org.springframework.web.bind.annotation.*
import java.io.File
import net.sf.jasperreports.engine.export.FileHtmlResourceHandler
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter

@RestController
@RequestMapping("/jasperservice")

class ServiceController
{
    @GetMapping("Test")
    fun SayHello() = "Hello"

    @PostMapping("/CreatePdfReport")
    fun SavePdfReport(@RequestBody parameters: ReportParameters) : Any?
    {
        println("Save PDF report called")
        println(parameters)
        var dbParameters = jacksonObjectMapper().readValue<DatabaseCredentials>(File(parameters.ReportDBConfPath!!))
        var connection = Helpers.PrepareConnection(dbParameters)
        return try {
            println(parameters.ReportPath)
            var compiledReport = JasperCompileManager.compileReport(parameters.ReportPath)
            var filledReport = JasperFillManager.fillReport(compiledReport, parameters.ReportParams, connection)
            var exporter = JRPdfExporter()
            exporter.setExporterInput(SimpleExporterInput(filledReport))
            var configuration = SimplePdfReportConfiguration()
            var pagecount = filledReport.pages.count()

            for (i in 0 until pagecount)
            {
                configuration.pageIndex = i
                exporter.setConfiguration(configuration)
                exporter.setExporterOutput(SimpleOutputStreamExporterOutput(File(String.format("${parameters.ReportSavePath}${parameters.ReportSaveName}_%d.pdf",i))))
                exporter.exportReport()
            }


            ResponseEntity("Saved to: ${parameters.ReportSavePath}", HttpStatus.OK)

        }
        catch (e: Exception) {
            ResponseEntity("Error: ${e.message}", HttpStatus.BAD_REQUEST)
        }

    }


    @PostMapping("/CreateHtmlReport")
    fun SaveHtmlReport(@RequestBody parameters: ReportParameters) : Any?
    {
        println("Save HTML report called")
        println(parameters)
        var dbParameters = jacksonObjectMapper().readValue<DatabaseCredentials>(File(parameters.ReportDBConfPath!!))
        var connection = Helpers.PrepareConnection(dbParameters)
        return try {
            var compiledReport = JasperCompileManager.compileReport(parameters.ReportPath)
            var filledReport = JasperFillManager.fillReport(compiledReport, parameters.ReportParams, connection)
            var exporter = HtmlExporter()
            exporter.setExporterInput(SimpleExporterInput(filledReport))
            var configuration = SimpleHtmlReportConfiguration()


            for (i in 0 until filledReport.pages.count())
            {
                configuration.pageIndex = i
                var exporterOutput = SimpleHtmlExporterOutput(File(String.format("${parameters.ReportSavePath}${parameters.ReportSaveName}_%d.html",i)))
//                exporterOutput.imageHandler = FileHtmlResourceHandler(File(String.format("${parameters.ReportSavePath}/${parameters.ReportSaveName}_%d_files",i)))
                exporterOutput.imageHandler = FileHtmlResourceHandler(File(String.format("${parameters.ReportSavePath}/${parameters.ReportSaveName}_%d_files", i)), String.format("${parameters.ReportSaveName}_%d_files", i)+"/{0}")
//                exporterOutput.setImageHandler(FileHtmlResourceHandler(File("html_images"), "html_images/{0}"))
                exporter.setConfiguration(configuration)

                exporter.setExporterOutput(exporterOutput)
                exporter.exportReport()
            }


            ResponseEntity("Saved to: ${parameters.ReportSavePath}", HttpStatus.OK)

            }
            catch (e: Exception) {
                ResponseEntity("Error: ${e.message}", HttpStatus.BAD_REQUEST)
            }
        }

    @PostMapping("CreateDocxReport")
    fun SaveDocxReport(@RequestBody parameters: ReportParameters) : Any?
    {
        println("Save DOCX report called")
        println(parameters)
        var dbParameters = jacksonObjectMapper().readValue<DatabaseCredentials>(File(parameters.ReportDBConfPath!!))
        var connection = Helpers.PrepareConnection(dbParameters)
        return try {
            println(parameters.ReportPath)
            var compiledReport = JasperCompileManager.compileReport(parameters.ReportPath)
            var filledReport = JasperFillManager.fillReport(compiledReport, parameters.ReportParams, connection)
            var exporter = JRDocxExporter()
            exporter.setExporterInput(SimpleExporterInput(filledReport))
            var configuration = SimpleDocxReportConfiguration()
            var pagecount = filledReport.pages.count()

            for (i in 0 until pagecount)
            {
                configuration.pageIndex = i
                exporter.setConfiguration(configuration)
                exporter.setExporterOutput(SimpleOutputStreamExporterOutput(File(String.format("${parameters.ReportSavePath}${parameters.ReportSaveName}_%d.docx",i))))
                exporter.exportReport()
            }

            ResponseEntity("Saved to: ${parameters.ReportSavePath}", HttpStatus.OK)

        }
        catch (e: Exception) {
            ResponseEntity("Error: ${e.message}", HttpStatus.BAD_REQUEST)
        }

    }


    @PostMapping("CreateCsvReport")
    fun SaveCsvReport(@RequestBody parameters: ReportParameters) : Any?
    {
        println("Save CSV report called")
        println(parameters)
        var dbParameters = jacksonObjectMapper().readValue<DatabaseCredentials>(File(parameters.ReportDBConfPath!!))
        var connection = Helpers.PrepareConnection(dbParameters)
        return try {
            println(parameters.ReportPath)
            var compiledReport = JasperCompileManager.compileReport(parameters.ReportPath)
            var filledReport = JasperFillManager.fillReport(compiledReport, parameters.ReportParams, connection)
            var exporter = JRCsvExporter()
            exporter.setExporterInput(SimpleExporterInput(filledReport))
            var configuration = SimpleCsvReportConfiguration()
            var pagecount = filledReport.pages.count()

            for (i in 0 until pagecount)
            {
                configuration.pageIndex = i
                exporter.setConfiguration(configuration)
                exporter.setExporterOutput(SimpleWriterExporterOutput(File(String.format("${parameters.ReportSavePath}${parameters.ReportSaveName}_%d.csv",i))))
                exporter.exportReport()
            }

            ResponseEntity("Saved to: ${parameters.ReportSavePath}", HttpStatus.OK)

        }
        catch (e: Exception) {
            ResponseEntity("Error: ${e.message}", HttpStatus.BAD_REQUEST)
        }
    }

    @PostMapping("CreateRtfReport")
    fun SaveRtfReport(@RequestBody parameters: ReportParameters) : Any?
    {
        println("Save RTF report called")
        println(parameters)
        var dbParameters = jacksonObjectMapper().readValue<DatabaseCredentials>(File(parameters.ReportDBConfPath!!))
        var connection = Helpers.PrepareConnection(dbParameters)
        return try {
            println(parameters.ReportPath)
            var compiledReport = JasperCompileManager.compileReport(parameters.ReportPath)
            var filledReport = JasperFillManager.fillReport(compiledReport, parameters.ReportParams, connection)
            var exporter = JRRtfExporter()
            exporter.setExporterInput(SimpleExporterInput(filledReport))
            var configuration = SimpleRtfReportConfiguration()
            var pagecount = filledReport.pages.count()

            for (i in 0 until pagecount)
            {
                configuration.pageIndex = i
                exporter.setConfiguration(configuration)
                exporter.setExporterOutput(SimpleWriterExporterOutput(File(String.format("${parameters.ReportSavePath}${parameters.ReportSaveName}_%d.rtf",i))))
                exporter.exportReport()
            }

            ResponseEntity("Saved to: ${parameters.ReportSavePath}", HttpStatus.OK)

        }
        catch (e: Exception) {
            ResponseEntity("Error: ${e.message}", HttpStatus.BAD_REQUEST)
        }

    }

    @PostMapping("CreateXlsReport")
    fun SaveXlsReport(@RequestBody parameters: ReportParameters) : Any?
    {
        println("Save XLS report called")
        println(parameters)
        var dbParameters = jacksonObjectMapper().readValue<DatabaseCredentials>(File(parameters.ReportDBConfPath!!))
        var connection = Helpers.PrepareConnection(dbParameters)
        return try {
            println(parameters.ReportPath)
            var compiledReport = JasperCompileManager.compileReport(parameters.ReportPath)
            var filledReport = JasperFillManager.fillReport(compiledReport, parameters.ReportParams, connection)
//            var exporter = JRXlsExporter()
//            exporter.setExporterInput(SimpleExporterInput(filledReport))
//            var configuration = SimpleXlsReportConfiguration()
//            configuration.isDetectCellType = true
//            configuration.isCollapseRowSpan = false
//            configuration.isOnePagePerSheet = true
//            configuration.isForcePageBreaks = true
//            configuration.isWhitePageBackground = false

            var pagecount = filledReport.pages.count()

            for (i in 0 until pagecount)
            {
                var exporter = JRXlsExporter()
                exporter.setExporterInput(SimpleExporterInput(filledReport))
                var configuration = SimpleXlsReportConfiguration()

                configuration.isDetectCellType = true
                configuration.isCollapseRowSpan = false
                configuration.isOnePagePerSheet = true
                configuration.pageIndex = i

                exporter.setConfiguration(configuration)
                exporter.setExporterOutput(SimpleOutputStreamExporterOutput(File(String.format("${parameters.ReportSavePath}${parameters.ReportSaveName}_%d.xls",i))))
                exporter.exportReport()
            }

//            exporter.setExporterOutput(SimpleOutputStreamExporterOutput(File("${parameters.ReportSavePath}${parameters.ReportSaveName}.xls")))
//            exporter.exportReport()

            ResponseEntity("Saved to: ${parameters.ReportSavePath}", HttpStatus.OK)

        }
        catch (e: Exception) {
            ResponseEntity("Error: ${e.message}", HttpStatus.BAD_REQUEST)
        }

    }
}